import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { GlobalVariablesService } from '../global/global-variables.service';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
// import 'rxjs/add/operator/throw';

@Injectable()
export class PayumoneyService {
  payment_url:string = this.global.SERVER_URL + '/payment/make';

  constructor(private global:GlobalVariablesService, private _http:HttpClient) { }

  // get HTML form of payment to send payment request
  getPaymentForm(values:Object): Observable<any> {
    // get payment data
    return this._http.post (this.payment_url, values, {
      responseType: 'text'
    })
          .do((data) => {
            // console.log (data);
          })
          .catch(this.handleError);
  }

  private handleError (err:HttpErrorResponse) {
    // console.log (err);
    return Observable.throw (err.message);
  }
}
