import { TestBed, inject } from '@angular/core/testing';

import { PayumoneyService } from './payumoney.service';

describe('PayumoneyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PayumoneyService]
    });
  });

  it('should be created', inject([PayumoneyService], (service: PayumoneyService) => {
    expect(service).toBeTruthy();
  }));
});
