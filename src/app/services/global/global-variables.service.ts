import { Injectable } from '@angular/core';

@Injectable()
export class GlobalVariablesService {

  constructor() { }

  public SITE_BASE_NAME:String = "Open Eyes Foundation";
  public SITE_LOCATION:String = "Manimajra, Chandigarh";

  public SERVER_URL:String = "https://localhost:3000";

}
