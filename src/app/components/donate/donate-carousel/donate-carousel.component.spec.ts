import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DonateCarouselComponent } from './donate-carousel.component';

describe('DonateCarouselComponent', () => {
  let component: DonateCarouselComponent;
  let fixture: ComponentFixture<DonateCarouselComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DonateCarouselComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DonateCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
