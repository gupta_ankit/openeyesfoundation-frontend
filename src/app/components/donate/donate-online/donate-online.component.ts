import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, NgForm, AbstractControl } from '@angular/forms';

// pay online service
import { PayumoneyService } from '../../../services/payment/payumoney.service';

@Component({
  selector: 'app-donate-online',
  templateUrl: './donate-online.component.html',
  styleUrls: ['./donate-online.component.css'],
  providers: [PayumoneyService]
})
export class DonateOnlineComponent implements OnInit {
  error_message: any;

  showDonationForm:Boolean = true;

  donation:FormGroup;
  donationForm:any;

  constructor (private payment:PayumoneyService) {}

  ngOnInit() {
    this.validateDonationForm ();
  }

  /**
   * Design Validation for donation form
   */
  validateDonationForm (): void {
    this.donation = new FormGroup({
      firstname: new FormControl ('Test', [Validators.required, Validators.maxLength (50), Validators.pattern (/^[a-zA-Z\s]{1,50}$/)]),
      amount: new FormControl (1, [Validators.required, Validators.maxLength (6), Validators.pattern (/^[0-9]{1,6}/)]),
      email: new FormControl ('testdev.kis@gmail.com', [Validators.required, Validators.maxLength (255), Validators.email]),
      phone: new FormControl ('9653836834', [Validators.required, Validators.maxLength (15), Validators.pattern (/^[0-9]{10,15}/)])      
    });
  }

  /**
   * Action on donation form submit
   */
  donationSubmit (form:NgForm) {
    let values = {
      firstname: (form.controls.firstname.value),
      amount: (form.controls.amount.value),
      email: (form.controls.email.value),
      phone: (form.controls.phone.value)
    }

    this.payment.getPaymentForm (values).subscribe (res => {
      this.showDonationForm = false;
      this.donationForm = res;
      console.log (res);
    }, err => {
      console.log ('error');
      console.log (err);
    });
  }

  /**
   * Method to reset payment values
   */
  resetPayment():void {
    this.showDonationForm = true;
    this.donationForm = null;
  }
}
