import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DonateOnlineComponent } from './donate-online.component';

describe('DonateOnlineComponent', () => {
  let component: DonateOnlineComponent;
  let fixture: ComponentFixture<DonateOnlineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DonateOnlineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DonateOnlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
