import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DonateIntoAccountComponent } from './donate-into-account.component';

describe('DonateIntoAccountComponent', () => {
  let component: DonateIntoAccountComponent;
  let fixture: ComponentFixture<DonateIntoAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DonateIntoAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DonateIntoAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
