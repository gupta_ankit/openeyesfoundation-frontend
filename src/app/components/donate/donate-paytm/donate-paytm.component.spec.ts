import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DonatePaytmComponent } from './donate-paytm.component';

describe('DonatePaytmComponent', () => {
  let component: DonatePaytmComponent;
  let fixture: ComponentFixture<DonatePaytmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DonatePaytmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DonatePaytmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
