import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-image-gallery',
  templateUrl: './home-image-gallery.component.html',
  styleUrls: ['./home-image-gallery.component.css'],
})
export class HomeImageGalleryComponent implements OnInit {

  view_type:String = 'all';

  constructor() { }

  ngOnInit() {
  }

  viewGallery (type:String):void {
    this.view_type = type;
  }

}
