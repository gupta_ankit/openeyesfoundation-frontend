import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeQuoteCarouselComponent } from './home-quote-carousel.component';

describe('HomeQuoteCarouselComponent', () => {
  let component: HomeQuoteCarouselComponent;
  let fixture: ComponentFixture<HomeQuoteCarouselComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeQuoteCarouselComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeQuoteCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
