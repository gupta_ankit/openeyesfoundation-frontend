import { Component, OnInit } from '@angular/core';

import { GlobalVariablesService } from '../../services/global/global-variables.service';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css'],
  providers: [GlobalVariablesService]
})
export class AboutUsComponent implements OnInit {

  constructor(private global:GlobalVariablesService) { }

  SITE_BASE_NAME:String = this.global.SITE_BASE_NAME;
  SITE_LOCATION:String = this.global.SITE_LOCATION;

  ngOnInit() {
  }

}
