import { Component, OnInit } from '@angular/core';

import { GlobalVariablesService } from '../../services/global/global-variables.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers:[GlobalVariablesService]
})
export class HeaderComponent implements OnInit {

  constructor(private global:GlobalVariablesService) { }

  SITE_BASE_NAME:String = this.global.SITE_BASE_NAME;

  ngOnInit() {
  }

}
