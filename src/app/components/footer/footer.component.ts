import { Component, OnInit } from '@angular/core';

import { GlobalVariablesService } from '../../services/global/global-variables.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css'],
  providers:[GlobalVariablesService]
})
export class FooterComponent implements OnInit {

  constructor(private global:GlobalVariablesService) { }

  SITE_BASE_NAME:String = this.global.SITE_BASE_NAME;

  ngOnInit() {
  }

}
