import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RouterModule } from '@angular/router';

import { ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';

// components
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { HomeCarouselComponent } from './components/home/home-carousel/home-carousel.component';
import { HomeCardsComponent } from './components/home/home-cards/home-cards.component';
import { HomeImageGalleryComponent } from './components/home/home-image-gallery/home-image-gallery.component';
import { HomeTimeLineComponent } from './components/home/home-time-line/home-time-line.component';
import { HomeQuoteCarouselComponent } from './components/home/home-quote-carousel/home-quote-carousel.component';
import { DonateComponent } from './components/donate/donate.component';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { ImageGalleryComponent } from './components/image-gallery/image-gallery.component';
import { OurServicesComponent } from './components/our-services/our-services.component';
import { AboutUsOurTeamComponent } from './components/about-us/about-us-our-team/about-us-our-team.component';
import { RecentEventsComponent } from './components/image-gallery/recent-events/recent-events.component';
import { DonateCarouselComponent } from './components/donate/donate-carousel/donate-carousel.component';
import { DonateIntoAccountComponent } from './components/donate/donate-into-account/donate-into-account.component';
import { DonatePaytmComponent } from './components/donate/donate-paytm/donate-paytm.component';
import { DonateOnlineComponent } from './components/donate/donate-online/donate-online.component';
import { ShowErrorsComponent } from './components/show-errors/show-errors.component';
import { NumberOnlyDirective } from './directives/number/number-only.directive';

import { GlobalVariablesService } from './services/global/global-variables.service';
import { TrimInputDirective } from './directives/trim/trim-input.directive';
import { SafeHtmlPipe } from './pipes/html/safe-html.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    HomeCarouselComponent,
    HomeCardsComponent,
    HomeImageGalleryComponent,
    HomeTimeLineComponent,
    HomeQuoteCarouselComponent,
    DonateComponent,
    AboutUsComponent,
    ImageGalleryComponent,
    OurServicesComponent,
    AboutUsOurTeamComponent,
    RecentEventsComponent,
    DonateCarouselComponent,
    DonateIntoAccountComponent,
    DonatePaytmComponent,
    DonateOnlineComponent,
    ShowErrorsComponent,
    NumberOnlyDirective,
    TrimInputDirective,
    SafeHtmlPipe,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot ([
      {
        path: '',
        component: HomeComponent
      },{
        path: 'donate',
        component: DonateComponent
      }, {
        path: 'about',
        component: AboutUsComponent
      }, {
        path: 'gallery',
        component: ImageGalleryComponent
      }, {
        path: 'services',
        component: OurServicesComponent
      }
    ])
  ],
  providers: [GlobalVariablesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
