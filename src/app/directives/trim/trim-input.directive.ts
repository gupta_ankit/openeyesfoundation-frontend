import { Directive, ElementRef, ChangeDetectorRef } from '@angular/core';

@Directive({
  selector: 'appTrimInput',
  host: {'(blur)': 'onchange($event)'}
})
export class TrimInputDirective {
  constructor(private cdRef: ChangeDetectorRef, private el: ElementRef) { }

  onChange($event: any) {
    let theEvent = $event || window.event;
    theEvent.target.value = theEvent.target.value.trim();
  }
}
